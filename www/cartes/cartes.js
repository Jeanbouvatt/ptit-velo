


// Fonction d'initialisation de la carte
function initMap() {
    var lonLondres = 45.1840971;
    var latLondres = 5.7153318;

    var lonCampus = 45.18920;
    var latCampus = 5.76891;

    var attribution = '© contributeurs d\'<a href="https://osm.org/copyright">OpenStreetMap</a> - rendu <a href="https://openstreetmap.fr">OSM France</a>';
    var tile_url = 'https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png';
    

    var carte = L.map('carte');
    carte.scrollWheelZoom.disable();
    
    L.tileLayer(tile_url, {attribution: attribution, minZoom: 10, maxZoom: 20}).addTo(carte);
   
    var marqueurAtelier = L.icon({
      iconUrl: "cartes/marker.png",
      shadowUrl: "cartes/marker-shadow.png",
      iconSize: [25, 41],
      shadowSize:   [41, 41],
      iconAnchor: [12, 41],
      shadowAnchor: [12, 41],
      popupAnchor: [0, -41],
    });

    var markerLondres = L.marker([lonLondres, latLondres],{ icon: marqueurAtelier })
        .addTo(carte)
        .bindPopup("<h4>Atelier de Londres</h4><p>5 bis rue de Londres, 38000 Grenoble<br />04.76.21.46.01</p>");
        
    var markerCampus = L.marker([lonCampus, latCampus],{ icon: marqueurAtelier })
        .addTo(carte)
        .bindPopup("<h4>Atelier du campus</h4><p>921 rue des résidences, 38400 Saint-Martin-D’Hères<br />04.76.54.61.09</p>");
        
    var margin = 0.005;
    var bounds = new L.LatLngBounds([[lonLondres-margin, latLondres-margin],[lonCampus+margin, latCampus+margin]]);
    carte.fitBounds(bounds);
}


window.onload = function(){
  // Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
  initMap(); 
};
