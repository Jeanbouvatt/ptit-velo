# ptit-velo

Dépot du code source du ptit vélo https://www.ptitvelo.net/

Il s'agit d'un site static, rien de plus
## SSH
Le déploiement se fait via sftp, il faut une clé SSH.
pour ça il faut modifier /.ssh/authorized_keys sur le serveur distant pour rajouter la clé **publique**, il faut donc 
déjà un accès SSH, ou alors contacter l'heureux cyclage qui gère l'hébergement

## Déploiement
```
sftp www-ptitvelo-www@ptitvelo.net
sftp> put www/index.html www/index.html
```
